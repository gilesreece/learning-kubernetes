# 01 Namespaces

## Creating your namespace

To create your namespace firstly open `namespace.yaml`

Make the change to the yaml file to create a namespace with your name.

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: <namespace>
```

Once done, this can now be applied to Kubernetes:

```bash
cd 01-namespace
kubectl apply -f namespace.yaml
```