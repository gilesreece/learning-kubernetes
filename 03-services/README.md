# 03 Services

## Creating your 1st service

To create your Service firstly open `service.yaml`

Make the change to the yaml file to create a service with your name.

```yaml
apiVersion: v1
kind: Service
metadata:
  name: <service name>
spec:
  ports:
    - port: 80
      protocol: TCP
  selector:
    app: <pod label name>
```

Once done, this can now be applied to Kubernetes:

```bash
cd 03-services
kubectl apply -f service.yaml
```