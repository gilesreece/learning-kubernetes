# 02 Pods

## Creating your 1st pod

To create your Pod firstly open `pod.yaml`

Make the change to the yaml file to create a pod with your name.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: <pod name>
  labels:
    app: <label name>
spec:
  containers:
    - name: <pod name>
      image: nginx:latest
      ports:
        - containerPort: 80
```

Once done, this can now be applied to Kubernetes:

```bash
cd 02-pods
kubectl apply -f pods.yaml
```